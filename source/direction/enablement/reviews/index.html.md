---
layout: markdown_page
title: Enablement Direction Page reviews
description: "The bi-weekly direction review schedule."
canonical_path: "/direction/enablement/reviews/"
---

## What's on this page

This page contains the bi-weekly Enablement & SaaS Platforms section direction review. Every other week, the PM teams reviews one of the groups direction pages asynchronously and adds their questions to the weekly PM agenda discussion. During the weekly meeting, these questions are discussed synchronously and recorded separately from the main meeting.

### Schedule

| Group | Date | PM | Recording |
| ----- | ---- | -- | --------- |
| [Global Search](https://about.gitlab.com/direction/global-search/) | 2022-06-15 | [John McGuire](https://gitlab.com/johnmcguire) | N/A |
| [Distribution] | 2022-06-29 | [Dilan Orrino](https://gitlab.com/dorrino) | N/A |
| [Geo](https://about.gitlab.com/direction/geo/) | 2022-07-13 | [Sampath Ranasinghe](https://gitlab.com/sranasinghe) | N/A |
| [Memory](https://about.gitlab.com/direction/memory/) | 2022-07-27 | [Yannis Roussos](https://gitlab.com/iroussos) | N/A |
| [Gitaly](https://about.gitlab.com/direction/gitaly/) | 2022-08-10 | [Mark Wood](https://gitlab.com/mjwood) | N/A |
| [Code Search Category](https://about.gitlab.com/direction/global-search/code-search/) | 2022-08-24 | [John McGuire](https://gitlab.com/johnmcguire) | N/A |
| [Database](https://about.gitlab.com/direction/database/) | 2022-09-07 | [Yannis Roussos](https://gitlab.com/iroussos) | N/A |
| [Sharding] | 2022-09-21 | [Fabian Zimmer](https://gitlab.com/fzimmer) | N/A |
| [GitLab Dedicated](https://about.gitlab.com/direction/saas-platforms/dedicated) | 2022-10-05 | [Andrew Thomas](https://gitlab.com/awthomas) | N/A |
